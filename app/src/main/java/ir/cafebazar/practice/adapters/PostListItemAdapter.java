package ir.cafebazar.practice.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import ir.cafebazar.practice.R;
import ir.cafebazar.practice.activities.AbstractListAdapter;
import ir.cafebazar.practice.broadcast.ApplicationData;
import ir.cafebazar.practice.util.CircularImageView;


public class PostListItemAdapter extends AbstractListAdapter<String> {

    private final Context mContext;
    private final DisplayImageOptions iconOptions;
    private final DisplayImageOptions imageOptions;
    private ImageLoadingListener animateFirstListener;

    public PostListItemAdapter(Context context) {
        super(context);
        mContext = context;
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(mContext));

        imageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img_load)
                .showImageOnFail(R.drawable.img_load)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
        iconOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_anonymous_user)
                .showImageOnFail(R.drawable.profile_anonymous_user)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }



    @Override
    public Object getItem(int section, int position) {
        return null;
    }

    @Override
    public long getItemId(int section, int position) {
        return 0;
    }

    @Override
    public int getSectionCount() {
        return 33;
    }

    @Override
    public int getCountForSection(int section) {
        return 1;
    }


    @Override
    public View getItemView(int section, int position, View convertView, ViewGroup parent) {
        final String url = this.getItem(section);
        LinearLayout layout = null;
        if (convertView == null) {
            LayoutInflater inflator = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = (LinearLayout) inflator.inflate(R.layout.adapter_post, null);
        } else {
            layout = (LinearLayout) convertView;
        }
        ImageView userIv = (ImageView) layout.findViewById(R.id.image);
        setImageView(userIv, url);

        return layout;
    }

    @Override
    public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {
        final String url = this.getItem(section);
        LinearLayout layout = null;
        if (convertView == null) {
            LayoutInflater inflator = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = (LinearLayout) inflator.inflate(R.layout.adapter_header, null);
        } else {
            layout = (LinearLayout) convertView;
        }
        ((TextView)layout.findViewById(R.id.name)).setText(ApplicationData.session.getUser().username);
        CircularImageView userIv = (CircularImageView) layout.findViewById(R.id.icon);
        setIconView(userIv, ApplicationData.session.getUser().profilPicture);
        return layout;
    }

    private void setImageView(ImageView viewHolder, String URL) {
        animateFirstListener = new AnimateFirstDisplayListener();
        ImageLoader.getInstance().displayImage(URL,
                viewHolder, imageOptions, animateFirstListener);
    }
    private void setIconView(ImageView viewHolder, String URL) {
        animateFirstListener = new AnimateFirstDisplayListener();
        ImageLoader.getInstance().displayImage(URL,
                viewHolder, iconOptions, animateFirstListener);
    }
    public static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }

        }
    }

}
