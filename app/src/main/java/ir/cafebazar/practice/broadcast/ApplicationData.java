package ir.cafebazar.practice.broadcast;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import net.londatiga.android.instagram.InstagramSession;

public class ApplicationData {
    public static final String CLIENT_ID = "72392273dfca467291fc449e62908e75";
    public static final String CLIENT_SECRET = "7e509f32d1ca456183a3c257c437d0df";
	public static final String CALLBACK_URL = "instagram://connect";

    public static InstagramSession session = null;
    public static String[] media;

    public static boolean isNetworkAvailable(Context context){
        ConnectivityManager connectivityManager = null;
        try {
            connectivityManager
                    = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        }catch (Exception ignored){}
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
