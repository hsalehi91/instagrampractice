package ir.cafebazar.practice.broadcast;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.File;
import java.io.IOException;

import ir.cafebazar.practice.R;
import ir.cafebazar.practice.util.FileDownloader;

public class NewVersionCheckerService extends BroadcastReceiver {

    private static final int NOTIFICATION_ID = 100;

    @Override
    public void onReceive(Context context, Intent broadcastedIntent) {

        int versionCode;
        try {
            versionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
            return;
        }

        new CheckNewVersionTask().execute(context, versionCode);

    }

    class CheckNewVersionTask extends AsyncTask<Object, String, String> {

        private Context context;

        @Override
        protected String doInBackground(Object... args) {
            context = (Context) args[0];
            int versionCode = (Integer) args[1];
            return null;
//                    ServiceFactory.getInstance().getNewVersionUrl(versionCode);
        }

        @Override
        protected void onPostExecute(String newVersionUrl) {
            if (newVersionUrl == null || newVersionUrl.trim().length() == 0) {
                return;
            }
            Log.d("VersionCheckService", "New version available at this url: " + newVersionUrl);
            new FileDownloadTask().execute(context, newVersionUrl);
        }

    }

    class FileDownloadTask extends AsyncTask<Object, Void, File> {

        private Context context;

        @Override
        protected File doInBackground(Object... args) {
            this.context = (Context) args[0];
            String newVersionUrl = (String) args[1];
            try {
                File apk = FileDownloader.downloadFile(newVersionUrl);
                if (apk == null) {
                    Log.d("VersionCheckService", "apk not downloaded");
                    return null;
                }
                Log.d("VersionCheckService", "apk downloaded: " + apk);
                return apk;
            } catch (IOException e) {
                Log.w("VersionCheckService", "Error during download", e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(File apk) {

            if (apk == null) {
                return;
            }

            // onClick event
            // Intent downloadIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(newVersionUrl));
            Intent installIntent = new Intent(Intent.ACTION_VIEW);
            installIntent.setDataAndType(Uri.fromFile(apk), "application/vnd.android.package-archive");
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, installIntent, 0);

            // creating the notification
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setSmallIcon(R.drawable.ic_launcher);
            builder.setContentTitle(context.getResources().getString(R.string.update_notification_title));
            builder.setContentText(context.getResources().getString(R.string.update_notification_text));
            builder.setContentIntent(pendingIntent);
            Notification notification = builder.build();

            // showing the notification
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(NOTIFICATION_ID, notification);
        }
    }

}
