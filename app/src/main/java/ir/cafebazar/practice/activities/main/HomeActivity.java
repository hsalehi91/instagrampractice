package ir.cafebazar.practice.activities.main;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

import org.json.JSONException;

import ir.cafebazar.practice.R;
import ir.cafebazar.practice.activities.AbstractActivityHome;
import ir.cafebazar.practice.activities.AbstractAdapter;
import ir.cafebazar.practice.adapters.PostListItemAdapter;
import ir.cafebazar.practice.broadcast.ApplicationData;
import ir.cafebazar.practice.service.ServiceFactory;


public class HomeActivity extends AbstractActivityHome<String> {

    public HomeActivity() {
        curPage = 0;
        setEmptyTextResourceId(R.string.empty_comment);
        setPaginationEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //check for network availability
        if(!ApplicationData.isNetworkAvailable(this))
                Toast.makeText(HomeActivity.this, "خطای دسترسی به اینترنت", Toast.LENGTH_LONG).show();

        initiateFloatingActionBar(this);
        initiateMenuBar(this, view);
    }

    @Override
    protected AbstractAdapter<String> createAdapter(Context context) {
        return new PostListItemAdapter(context);
    }

    @Override
    protected String[] queryServer(Bundle args, int last) {
        try {
            //check network availability
            if(ApplicationData.isNetworkAvailable(this))
                //send request to get media
                return ApplicationData.media = ServiceFactory.getInstance().getUserSelfMedia(String.valueOf(last));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    /*
    * For main tabs of the application
    * */
    public static void initiateMenuBar(final Context context, View view){
        (view.findViewById(R.id.home)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });
        (view.findViewById(R.id.search)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SearchActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });
        (view.findViewById(R.id.repository)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, NotificationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });
        (view.findViewById(R.id.profile)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });
    }
    /*
    * creates the view of circular FloatingActionBar;
    * TODO: add functionality
    * */
    public static void initiateFloatingActionBar(Context context) {
        final ImageView fabContent = new ImageView(context);
        fabContent.setImageDrawable(context.getResources().getDrawable(R.drawable.add));

        FloatingActionButton darkButton = new FloatingActionButton.Builder(context)
                .setTheme(FloatingActionButton.THEME_DARK)
                .setContentView(fabContent)
                .setPosition(FloatingActionButton.POSITION_BOTTOM_CENTER)
                .build();

        SubActionButton.Builder rLSubBuilder = new SubActionButton.Builder(context)
                .setTheme(SubActionButton.THEME_DARK);
        ImageView rlIcon2 = new ImageView(context);
        ImageView rlIcon3 = new ImageView(context);

        rlIcon2.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action_camera));
        rlIcon3.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action_chat));

        final FloatingActionMenu rightLowerMenu = new FloatingActionMenu.Builder(context)
                .addSubActionView(rLSubBuilder.setContentView(rlIcon2).build())
                .addSubActionView(rLSubBuilder.setContentView(rlIcon3).build())
                .attachTo(darkButton)
                .build();

        // Listen menu open and close events to animate the button content view
        rightLowerMenu.setStateChangeListener(new FloatingActionMenu.MenuStateChangeListener() {
            @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
            @Override
            public void onMenuOpened(FloatingActionMenu menu) {
                // Rotate the icon of rightLowerButton 45 degrees clockwise
                fabContent.setRotation(0);
                PropertyValuesHolder pvhR = PropertyValuesHolder.ofFloat(View.ROTATION, 45);
                ObjectAnimator animation = ObjectAnimator.ofPropertyValuesHolder(fabContent, pvhR);
                animation.start();
            }

            @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
            @Override
            public void onMenuClosed(FloatingActionMenu menu) {
                // Rotate the icon of rightLowerButton 45 degrees counter-clockwise
                fabContent.setRotation(45);
                PropertyValuesHolder pvhR = PropertyValuesHolder.ofFloat(View.ROTATION, 0);
                ObjectAnimator animation = ObjectAnimator.ofPropertyValuesHolder(fabContent, pvhR);
                animation.start();
            }
        });
    }
}
