package ir.cafebazar.practice.activities;

public interface Refreshable {

    void onRefreshButtonClick();

}
