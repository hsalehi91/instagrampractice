package ir.cafebazar.practice.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;

import java.util.Date;

import ir.cafebazar.practice.R;
import ir.cafebazar.practice.util.ArrayUtil;
import ir.cafebazar.practice.util.pinnedheaderlistview.PinnedHeaderListView;
import ir.cafebazar.practice.util.simplequickreturn.SimpleQuickReturn;


public abstract class AbstractActivityHome<T> extends SherlockFragmentActivity implements
        LoaderManager.LoaderCallbacks<T[]>, Refreshable{

    private AbstractAdapter<T> mAdapter;

    private Integer emptyTextResourceId;
    private boolean paginationEnabled;
    private boolean enableScroll = true;
    private Date now = new Date();
    protected int curPage = 0;

    private boolean mListShown;
    private View mProgressContainer;
    private View mListContainer;
    public SwipeRefreshLayout mPullRefreshListView;
    public PinnedHeaderListView actualListView;


    private OnListViewScrollListener mListViewScrollListener;
    private int mLastFirstVisibleItem;
    private int mLastTop;
    private int mScrollPosition;
    private int mLastHeight;
    public View view;

    public interface OnListViewScrollListener {
        void onScrollUpDownChanged(int delta, int scrollPosition, boolean exact);
        void onScrollIdle();
    }
    public void setOnScrollUpAndDownListener(OnListViewScrollListener listener) {
        this.mListViewScrollListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //set view for SimpleQuickReturn as Actionbar
        view = new SimpleQuickReturn(this)
                .setContent(R.layout.activity_home)
                .setHeader(R.layout.header_home)
                .createView();
        setContentView(view);

        actualListView = (PinnedHeaderListView)findViewById(R.id.list);

        (findViewById(R.id.internalEmpty)).setVisibility(View.GONE);
        mListContainer = findViewById(R.id.listContainer);
        mProgressContainer = findViewById(R.id.progressContainer);
        mListShown = true;


        mPullRefreshListView = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);

        Context context = this;
        mAdapter = createAdapter(context);
        actualListView.setAdapter(mAdapter);

        mPullRefreshListView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mPullRefreshListView.setRefreshing(false);
                        onRefreshButtonClick();
                    }
                }, 1000);
            }
        });



        getSupportLoaderManager().initLoader(0, null, this);

        //set Scroll Listener for end of the list load management, and both SimpleQuickReturn & StickyHeaderListView
        actualListView.setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalCount) {
                if (!paginationEnabled || !enableScroll) {
                    return;
                }
                if (firstVisibleItem + visibleItemCount >= totalCount && totalCount < 66) {
                    curPage++;
                    enableScroll = false;
                    setListShown(false);
                    Bundle args = new Bundle();
                    getSupportLoaderManager().restartLoader(0, args, AbstractActivityHome.this);
                    return;
                }
                if (mAdapter == null || mAdapter.getCount() == 0 || !actualListView.mShouldPin || (firstVisibleItem < actualListView.getHeaderViewsCount())) {
                    actualListView.mCurrentHeader = null;
                    actualListView.mHeaderOffset = 0.0f;
                    for (int i = firstVisibleItem; i < firstVisibleItem + visibleItemCount; i++) {
                        View header = actualListView.getChildAt(i);
                        if (header != null) {
                            header.setVisibility(View.VISIBLE);
                        }
                    }
                    return;
                }

                firstVisibleItem -= actualListView.getHeaderViewsCount();

                int section = mAdapter.getSectionForPosition(firstVisibleItem);
                int viewType = mAdapter.getSectionHeaderViewType(section);
                actualListView.mCurrentHeader = actualListView.getSectionHeaderView(section, actualListView.mCurrentHeaderViewType != viewType ? null : actualListView.mCurrentHeader);
                actualListView.ensurePinnedHeaderLayout(actualListView.mCurrentHeader);
                actualListView.mCurrentHeaderViewType = viewType;

                actualListView.mHeaderOffset = 0.0f;

                for (int i = firstVisibleItem; i < firstVisibleItem + visibleItemCount; i++) {
                    if (mAdapter.isSectionHeader(i)) {
                        View header = actualListView.getChildAt(i - firstVisibleItem);
                        float headerTop = header.getTop();
                        float pinnedHeaderHeight = actualListView.mCurrentHeader.getMeasuredHeight();
                        header.setVisibility(View.VISIBLE);
                        if (pinnedHeaderHeight >= headerTop && headerTop > 0) {
                            actualListView.mHeaderOffset = headerTop - header.getHeight();
                        } else if (headerTop <= 0) {
                            header.setVisibility(View.INVISIBLE);
                        }
                    }
                }
                actualListView.invalidate();

                View firstChild = view.getChildAt(0);
                if (firstChild == null) {
                    return;
                }

                int top = firstChild.getTop();
                int height = firstChild.getHeight();
                int delta;
                int skipped = 0;
                if (mLastFirstVisibleItem == firstVisibleItem) {
                    delta = mLastTop - top;
                } else if (firstVisibleItem > mLastFirstVisibleItem) {
                    skipped = firstVisibleItem - mLastFirstVisibleItem - 1;
                    delta = skipped * height + mLastHeight + mLastTop - top;
                } else {
                    skipped = mLastFirstVisibleItem - firstVisibleItem - 1;
                    delta = skipped * -height + mLastTop - (height + top);
                }
                boolean exact = skipped > 0;
                mScrollPosition += -delta;
                if (mListViewScrollListener != null) {
                    mListViewScrollListener.onScrollUpDownChanged(-delta, mScrollPosition, exact);
                }
                mLastFirstVisibleItem = firstVisibleItem;
                mLastTop = top;
                mLastHeight = firstChild.getHeight();



            }
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
                if (mListViewScrollListener != null && i == SCROLL_STATE_IDLE) {
                    mListViewScrollListener.onScrollIdle();
                }
            }

        });

        SimpleQuickReturn.mListView.getViewTreeObserver().addOnGlobalLayoutListener(SimpleQuickReturn.mOnGlobalLayoutListener);
        setOnScrollUpAndDownListener(new OnListViewScrollListener() {
            @Override
            public void onScrollUpDownChanged(int delta, int scrollPosition, boolean exact) {
                SimpleQuickReturn.onNewScroll(delta);
                SimpleQuickReturn.snap(SimpleQuickReturn.mHeaderTop == scrollPosition);
            }

            @Override
            public void onScrollIdle() {
                SimpleQuickReturn.onScrollIdle();
            }
        });


    }



    @Override
    public Loader<T[]> onCreateLoader(int id, final Bundle args) {
        Context context = this;

        return new AbstractLoader<T[]>(context, args, curPage) {

            @Override
            protected T[] queryServer(Bundle args, int last) {
                return AbstractActivityHome.this.queryServer(args, last);
            }

        };
    }

    @Override
    public void onLoadFinished(Loader<T[]> loader, T[] data) {

        boolean initiallyEmptyList = (mAdapter.getData() == null || mAdapter.getData().length == 0);
        (findViewById(R.id.internalEmpty)).setVisibility(View.GONE);
        mProgressContainer.setVisibility(View.GONE);

        if (!ArrayUtil.isEmpty(data)) {

            data = ArrayUtil.removeNulls(data);

            if (!ArrayUtil.isEmpty(data)) {

                mAdapter.addData(data);
                mAdapter.notifyDataSetChanged();
                // data returned and the last item is not null, so enable future loadings
                enableScroll = data[data.length - 1] != null;
            }else{
                enableScroll = false;
            }

        }

        Object[] allData = mAdapter.getData(); // new data + previous data

        if (ArrayUtil.isEmpty(allData) && emptyTextResourceId != null) {

            // data is empty
            (findViewById(R.id.internalEmpty)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.internalEmpty)).setText(Html.fromHtml(getResources().getString(emptyTextResourceId))); // show empty menu_list text
            setListShown(false); // hide spinner

        } else {

            // The menu_list should now be shown.
            if (initiallyEmptyList) {
                setListShown(true);
            } else {
                setListShownNoAnimation(true);
            }

            actualListView.scrollBy(0, 1);
        }



    }

    @Override
    public void onLoaderReset(Loader<T[]> loader) {
        // Clear the data in the adapter.
        mAdapter.setData(null);
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void onRefreshButtonClick() {
        curPage = 0;
        now = new Date();
        mAdapter.setData(null);
        mAdapter.notifyDataSetChanged();
        mProgressContainer.setVisibility(View.VISIBLE);
        setListShown(true);
        enableScroll = true;
        getSupportLoaderManager().restartLoader(0, null, AbstractActivityHome.this);
    }


    public void setEmptyTextResourceId(Integer emptyTextResourceId) {
        this.emptyTextResourceId = emptyTextResourceId;
    }

    public void setPaginationEnabled(boolean paginationEnabled) {
        this.paginationEnabled = paginationEnabled;
    }

    protected AbstractAdapter<T> getAdapter() {
        return mAdapter;
    }

    protected Date getStartDate(){
        return now;
    }

    protected abstract AbstractAdapter<T> createAdapter(Context context);

    protected abstract T[] queryServer(Bundle args, int last);


    public void setListShown(boolean shown, boolean animate) {

        if (mListShown == shown) {
            return;
        }
        mListShown = shown;
        if (shown) {
            if (animate) {
                mProgressContainer.startAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out));
                mListContainer.startAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
            }
            mListContainer.setVisibility(View.VISIBLE);
        } else {
            if (animate) {
                mProgressContainer.startAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
                // mListContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_out));
            }
            mProgressContainer.setVisibility(View.VISIBLE);
        }
    }

    public void setListShown(boolean shown) {
        setListShown(shown, true);
    }

    public void setListShownNoAnimation(boolean shown) {
        setListShown(shown, false);
    }

}
