package ir.cafebazar.practice.activities;

import ir.cafebazar.practice.util.ArrayUtil;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;

public abstract class AbstractListAdapter<T> extends AbstractAdapter<T> {

    private Context context;
    private T[] data;

    public AbstractListAdapter(Context context) {
        this.context = context;
    }

    public void addData(T[] data) {
        this.data = ArrayUtil.concat(this.data, data);
    }

    @Override
    public void setData(T[] data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        if (data == null) {
            return 0;
        }
        return data.length;
    }

    @Override
    public T getItem(int index) {
        return data[index];
    }

    @Override
    public long getItemId(int index) {
        return index;
    }

    protected Context getContext() {
        return context;
    }

    protected LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(context);
    }

    protected Resources getResources() {
        return context.getResources();
    }

    @Override
    public T[] getData() {
        return data;
    }

}
