package ir.cafebazar.practice.activities.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;

import ir.cafebazar.practice.R;
import ir.cafebazar.practice.activities.fragments.UserPostsGridFragment;
import ir.cafebazar.practice.broadcast.ApplicationData;
import ir.cafebazar.practice.util.popUpMenu.MenuItem;
import ir.cafebazar.practice.util.popUpMenu.PopupMenu;


public class ProfileActivity extends SherlockFragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_profile, null);
        setContentView(view);

        TextView usernameView = (TextView) findViewById(R.id.username);
        // set a pop up menu for user self profile actions
        (view.findViewById(R.id.toggle)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu menu = new PopupMenu(ProfileActivity.this);
                menu.add(0, 0, "خروج");
                menu.setOnItemSelectedListener(new PopupMenu.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(MenuItem item) {
                        switch (item.getItemId()){
                            case 0:
                                ApplicationData.session.reset();
                                finish();
                                Intent intent = new Intent(ProfileActivity.this, WelcomeTabStripActivity.class);
                                startActivity(intent);
                        }
                    }
                });
                menu.show(view.findViewById(R.id.toggle), -250);
            }
        });

        usernameView.setText(ApplicationData.session.getUser().username);

        HomeActivity.initiateFloatingActionBar(this);
        HomeActivity.initiateMenuBar(this, view);

        Fragment fragment = new UserPostsGridFragment();

        FragmentManager fragmentManager = this.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_content, fragment).commit();


    }

}
