package ir.cafebazar.practice.activities;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockListFragment;

import ir.cafebazar.practice.R;

/**
 * A Sherlock menu_list fragment with customized view: "R.layout.fragment_list". The view is customized to support unlimited
 * lists.
 *
 */
public class CustomizedViewSherlockListFragment extends SherlockListFragment {

    private boolean mListShown;
    private View mProgressContainer;
    private View mListContainer;
    public SwipeRefreshLayout mPullRefreshListView;
    public ListView mListView;
    public TextView mInternalEmpty;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int INTERNAL_EMPTY_ID = 0x00ff0001;
        View view = inflater.inflate(R.layout.fragment_list, container, false);


        mListView = (ListView)view.findViewById(android.R.id.list);
        mInternalEmpty = (TextView)view.findViewById(R.id.internalEmpty);

        mInternalEmpty.setId(INTERNAL_EMPTY_ID);
        mListContainer = view.findViewById(R.id.listContainer);
        mProgressContainer = view.findViewById(R.id.progressContainer);
        mListShown = true;

        mPullRefreshListView = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);


        return view;
    }

    public void setListShown(boolean shown, boolean animate) {

        if (mListShown == shown) {
            return;
        }
        mListShown = shown;
        if (shown) {
            mProgressContainer.setVisibility(View.GONE);
            mListContainer.setVisibility(View.VISIBLE);
        } else {
            mProgressContainer.setVisibility(View.VISIBLE);
            mListContainer.setVisibility(View.INVISIBLE);
        }
    }

    public void setListShown(boolean shown) {
        setListShown(shown, true);
    }

    public void setListShownNoAnimation(boolean shown) {
        setListShown(shown, false);
    }

    public void onBackPressed() {
    }

}
