package ir.cafebazar.practice.activities.main;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;

import com.actionbarsherlock.app.SherlockFragmentActivity;

import ir.cafebazar.practice.R;
import ir.cafebazar.practice.activities.fragments.EmptyFragment;


public class NotificationActivity extends SherlockFragmentActivity {
    /*
    * it's an Empty Activity!!
    * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_notification, null);
        setContentView(view);
        HomeActivity.initiateFloatingActionBar(this);
        HomeActivity.initiateMenuBar(this, view);

        Fragment fragment = new EmptyFragment();

        FragmentManager fragmentManager = this.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_content, fragment).commit();



    }
}
