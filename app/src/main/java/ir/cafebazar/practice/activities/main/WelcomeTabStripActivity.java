package ir.cafebazar.practice.activities.main;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.astuetz.PagerSlidingTabStrip;

import ir.cafebazar.practice.R;
import ir.cafebazar.practice.activities.fragments.LoginFragment;
import ir.cafebazar.practice.activities.fragments.RegisterFragment;


public class WelcomeTabStripActivity extends SherlockFragmentActivity implements ViewPager.OnPageChangeListener{

    private int mainColor = 0xFF232728;
    private TextView loginView;
    private TextView registerView;
    private Typeface mType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mType = Typeface.createFromAsset(getAssets(), "bbcnasim.ttf");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_strip);

        loginView = (TextView) findViewById(R.id.loginView);
        registerView = (TextView) findViewById(R.id.registerView);
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        final ViewPager pager = (ViewPager) findViewById(R.id.pager);
        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager());

        pager.setAdapter(adapter);
        pager.setCurrentItem(1);
        loginView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pager.setCurrentItem(1);
            }
        });
        registerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pager.setCurrentItem(0);
            }
        });
        tabs.setIndicatorColor(mainColor);
        tabs.setIndicatorHeight(10);


        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        pager.setPageMargin(pageMargin);

        tabs.setViewPager(pager);

        tabs.setOnPageChangeListener(this);
        tabs.setUnderlineColor(0xF5F5F5);
    }




@Override
public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }
@Override
public void onPageSelected(int position) {
        switch (position){
            case 0:
                registerView.setTypeface(mType, 1);
                loginView.setTypeface(mType, 0);
                break;
            case 1:
                loginView.setTypeface(mType, 1);
                registerView.setTypeface(mType, 0);
                break;
        }
}

@Override
public void onPageScrollStateChanged(int state) {

        }



public class MyPagerAdapter extends FragmentPagerAdapter {

    private final String[] TITLES;
    private Fragment[] pages;



    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
        TITLES = new String[]{"ثبت نام", "ورود"};
        pages = new Fragment[TITLES.length];
        pages[0] = new RegisterFragment();
        pages[1] = new LoginFragment();

//            }
        Bundle bundle = new Bundle();
        for(int i = 0; i < TITLES.length; i++)
            pages[i].setArguments(bundle);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {


        return pages[position];

    }

}

}