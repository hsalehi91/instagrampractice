package ir.cafebazar.practice.activities;

import ir.cafebazar.practice.util.pinnedheaderlistview.SectionedBaseAdapter;

public abstract class AbstractAdapter<T> extends SectionedBaseAdapter {

    public abstract T[] getData();

    public abstract void setData(T[] data);

    public abstract void addData(T[] data);

}
