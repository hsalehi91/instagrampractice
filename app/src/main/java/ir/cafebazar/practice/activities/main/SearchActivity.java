package ir.cafebazar.practice.activities.main;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.astuetz.PagerSlidingTabStrip;

import ir.cafebazar.practice.R;
import ir.cafebazar.practice.activities.fragments.EmptyFragment;
import ir.cafebazar.practice.util.FontOverride;

public class SearchActivity extends SherlockFragmentActivity implements ViewPager.OnPageChangeListener{

    private int mainColor = 0xFF6dc268;
    private String mKeyword = "";
    private int mPosition = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FontOverride.setDefaultFont(this, "MONOSPACE", "bbcnasim.ttf");
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_search, null);
        setContentView(view);
        HomeActivity.initiateFloatingActionBar(this);
        HomeActivity.initiateMenuBar(this, view);

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        ViewPager pager = (ViewPager) findViewById(R.id.pager);

        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager(), mKeyword);

        pager.setAdapter(adapter);
        pager.setCurrentItem(mPosition);
        tabs.setIndicatorColor(mainColor);
        tabs.setIndicatorHeight(10);


        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        pager.setPageMargin(pageMargin);

        tabs.setViewPager(pager);

        tabs.setOnPageChangeListener(this);
        tabs.setUnderlineColor(0xF5F5F5);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }
    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }



    public class MyPagerAdapter extends FragmentPagerAdapter {

        private final String[] TITLES;
        private Fragment[] pages;

        public MyPagerAdapter(FragmentManager fm, String keyword) {
            super(fm);
            TITLES = new String[]{"پست", "کاربر"};
            pages = new Fragment[TITLES.length];
            pages[0] = new EmptyFragment();
            pages[1] = new EmptyFragment();

            Bundle bundle = new Bundle();
            bundle.putString("KEYWORD", keyword);
            for(int i = 0; i < TITLES.length; i++)
                pages[i].setArguments(bundle);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {


            return pages[position];

        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

    }

}