package ir.cafebazar.practice.activities.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Collections;

import ir.cafebazar.practice.R;
import ir.cafebazar.practice.adapters.PostGridItemAdapter;
import ir.cafebazar.practice.adapters.PostListItemAdapter;
import ir.cafebazar.practice.broadcast.ApplicationData;
import ir.cafebazar.practice.util.CircularImageView;
import ir.cafebazar.practice.util.HeaderGridView;
import ir.cafebazar.practice.util.ScaleImageView;


public class UserPostsGridFragment extends SherlockFragment {
    private final DisplayImageOptions imageOptions;

    private PostListItemAdapter.AnimateFirstDisplayListener animateFirstListener;

    public UserPostsGridFragment() {
        imageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_anonymous_user)
                .showImageOnFail(R.drawable.profile_anonymous_user)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //set GridView
        View view = inflater.inflate(R.layout.fragment_grid, container, false);
        //set Header for GridView
        LayoutInflater mInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewHeader = mInflater.inflate(R.layout.header_profile, null);

        HeaderGridView mGridView = (HeaderGridView) view.findViewById(R.id.gridView);
        ScaleImageView mLists = (ScaleImageView) viewHeader.findViewById(R.id.posts);
        ScaleImageView mGrids = (ScaleImageView) viewHeader.findViewById(R.id.grids);
        TextView aboutView = (TextView) viewHeader.findViewById(R.id.about);
        TextView internalEmpty = (TextView) view.findViewById(R.id.internalEmpty);
        final CheckBox followCheck = (CheckBox) viewHeader.findViewById(R.id.follow_button);
        CircularImageView iconView = (CircularImageView) viewHeader.findViewById(R.id.icon);

        //get Media
        ArrayList<String> photoList = new ArrayList<>();
        if(ApplicationData.media != null)
            Collections.addAll(photoList, ApplicationData.media);
        else
            internalEmpty.setText(Html.fromHtml(getResources().getString(R.string.empty_comment)));


        aboutView.setText(ApplicationData.session.getUser().fullName);
        setImageView(iconView, ApplicationData.session.getUser().profilPicture);
        mLists.setImageResource(R.drawable.posts);
        mGrids.setImageResource(R.drawable.answers_pressed);
        mLists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new UserPostsListFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.main_content, fragment).commit();
            }
        });
        followCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    followCheck.setText("دنبال شده");
                }
                else{
                    followCheck.setText("دنبال کنید");
                }

            }
        });
        //set Adapter and Header for GridView
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width 	= (int) Math.ceil((double) dm.widthPixels / 3);
        width=width-10;
        int height	= width;
        PostGridItemAdapter adapter = new PostGridItemAdapter(getActivity());
        adapter.setData(photoList);
        adapter.setLayoutParam(width, height);
        mGridView.addHeaderView(viewHeader);
        mGridView.setAdapter(adapter);

        return view;
    }

    private void setImageView(ImageView viewHolder, String URL) {
        animateFirstListener = new PostListItemAdapter.AnimateFirstDisplayListener();
        ImageLoader.getInstance().displayImage(URL,
                viewHolder, imageOptions, animateFirstListener);
    }

}
