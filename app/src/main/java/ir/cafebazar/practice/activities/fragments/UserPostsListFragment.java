package ir.cafebazar.practice.activities.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import ir.cafebazar.practice.R;
import ir.cafebazar.practice.activities.AbstractAdapter;
import ir.cafebazar.practice.activities.AbstractListFragment;
import ir.cafebazar.practice.adapters.PostListItemAdapter;
import ir.cafebazar.practice.broadcast.ApplicationData;
import ir.cafebazar.practice.util.CircularImageView;
import ir.cafebazar.practice.util.ScaleImageView;


public class UserPostsListFragment extends AbstractListFragment<String> {

    private final DisplayImageOptions imageOptions;
    private PostListItemAdapter.AnimateFirstDisplayListener animateFirstListener;

    public UserPostsListFragment() {
        setEmptyTextResourceId(R.string.empty_comment);
        setPaginationEnabled(true);
        imageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_anonymous_user)
                .showImageOnFail(R.drawable.profile_anonymous_user)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //inflate HeaderView
        LayoutInflater mInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewHeader = mInflater.inflate(R.layout.header_profile, null);

        ScaleImageView mGrids = (ScaleImageView) viewHeader.findViewById(R.id.grids);
        ScaleImageView mPosts = (ScaleImageView) viewHeader.findViewById(R.id.posts);
        TextView aboutView = (TextView) viewHeader.findViewById(R.id.about);
        final CheckBox followCheck = (CheckBox) viewHeader.findViewById(R.id.follow_button);
        CircularImageView iconView = (CircularImageView) viewHeader.findViewById(R.id.icon);

        aboutView.setText(ApplicationData.session.getUser().fullName);
        mGrids.setImageResource(R.drawable.answers);
        mPosts.setImageResource(R.drawable.posts_pressed);
        mGrids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new UserPostsGridFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.main_content, fragment).commit();
            }
        });
        setImageView(iconView, ApplicationData.session.getUser().profilPicture);
        followCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    followCheck.setText("دنبال شده");
                }
                else{
                    followCheck.setText("دنبال کنید");
                }

            }
        });

        //set Header for ListView
        mListView.addHeaderView(viewHeader);
        mListView.setAdapter(mAdapter);



    }

    @Override
    protected AbstractAdapter<String> createAdapter(Context context) {
        return new PostListItemAdapter(context);
    }


    @Override
    protected String[] queryServer(Bundle args, int last) {
        return ApplicationData.media;
    }

    @Override
    public void onBackPressed() {

    }

    private void setImageView(ImageView viewHolder, String URL) {
        animateFirstListener = new PostListItemAdapter.AnimateFirstDisplayListener();
        ImageLoader.getInstance().displayImage(URL,
                viewHolder, imageOptions, animateFirstListener);
    }
}
