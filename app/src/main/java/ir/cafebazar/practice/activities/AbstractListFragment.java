package ir.cafebazar.practice.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;

import java.util.Date;

import ir.cafebazar.practice.util.ArrayUtil;


public abstract class AbstractListFragment<T> extends CustomizedViewSherlockListFragment implements
        LoaderManager.LoaderCallbacks<T[]>, Refreshable, OnScrollListener{

    public AbstractAdapter<T> mAdapter;

    private Integer emptyTextResourceId;
    private boolean paginationEnabled;
    private boolean enableScroll = true;
    private Date now = new Date();
    protected int curPage = 0;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        // Create an empty adapter we will use to display the loaded data.
        Context context = getSherlockActivity();
        mAdapter = createAdapter(context);

        mPullRefreshListView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mPullRefreshListView.setRefreshing(false);
                        onRefreshButtonClick();
                    }
                }, 1000);
            }
        });


        // Start out with a progress indicator.
        setListShown(false);

        // Prepare the loader. Either re-connect with an existing one,
        // or start a new one.
        getLoaderManager().initLoader(0, null, this);

        mListView.setOnScrollListener(this);



    }


    @Override
    public Loader<T[]> onCreateLoader(int id, final Bundle args) {
        Context context = getSherlockActivity();
//        int last = 0;
//        if (args != null) {
//            last = 1+ args.getInt("last", 0)/10;
//        }
        return new AbstractLoader<T[]>(context, args, curPage) {

            @Override
            protected T[] queryServer(Bundle args, int last) {
                return AbstractListFragment.this.queryServer(args, last);
            }

        };
    }

    @Override
    public void onLoadFinished(Loader<T[]> loader, T[] data) {

        boolean initiallyEmptyList = (mAdapter.getData() == null || mAdapter.getData().length == 0);

        if (!ArrayUtil.isEmpty(data)) {

            data = ArrayUtil.removeNulls(data);

            if (!ArrayUtil.isEmpty(data)) {

                mAdapter.addData(data);
                mAdapter.notifyDataSetChanged();
                if (data[data.length - 1] != null) {
                    // data returned and the last item is not null, so enable future loadings
                    enableScroll = true;
                }
                else
                    enableScroll = false;
            }else{
                enableScroll = false;
            }

        }

        Object[] allData = mAdapter.getData(); // new data + previous data

        if (ArrayUtil.isEmpty(allData) && emptyTextResourceId != null) {

            // data is empty
            mInternalEmpty.setText(Html.fromHtml(getResources().getString(emptyTextResourceId))); // show empty menu_list text
            setListShown(true); // hide spinner

        } else {

            // The menu_list should now be shown.
            if (isResumed() && initiallyEmptyList) {
                setListShown(true);
            } else {
                setListShownNoAnimation(true);
            }

            mListView.scrollBy(0, 1);
        }


    }

    @Override
    public void onLoaderReset(Loader<T[]> loader) {
        // Clear the data in the adapter.
        mAdapter.setData(null);
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void onRefreshButtonClick() {
        curPage = 0;
        now = new Date();
        mAdapter.setData(null);
        mAdapter.notifyDataSetChanged();
        setListShown(true);
        enableScroll = true;
        getLoaderManager().restartLoader(0, null, AbstractListFragment.this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        OptionsMenuUtil.addRefreshButton(menu, this);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisible, int visibleCount, int totalCount) {
        if (!paginationEnabled || !enableScroll) {
            return;
        }
        if (firstVisible + visibleCount >= totalCount && totalCount<66) {
            curPage++;
            enableScroll = false;
            setListShown(false);
            Bundle args = new Bundle();
            //args.putInt("last", firstVisible + visibleCount);
            getLoaderManager().restartLoader(0, args, AbstractListFragment.this);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int s) {

    }

    public void setEmptyTextResourceId(Integer emptyTextResourceId) {
        this.emptyTextResourceId = emptyTextResourceId;
    }

    public void setPaginationEnabled(boolean paginationEnabled) {
        this.paginationEnabled = paginationEnabled;
    }

    protected AbstractAdapter<T> getAdapter() {
        return mAdapter;
    }

    protected Date getStartDate(){
        return now;
    }

    protected abstract AbstractAdapter<T> createAdapter(Context context);

    protected abstract T[] queryServer(Bundle args, int last);

    public abstract void onBackPressed();

}
