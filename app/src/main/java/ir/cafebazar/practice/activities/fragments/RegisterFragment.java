package ir.cafebazar.practice.activities.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import ir.cafebazar.practice.R;


public class RegisterFragment extends Fragment {
    /**
     * This is only a view for register procedure and does not have functionality
     *
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        return view;
    }

    public class UserRegisterTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(final String oath) {
            if (oath != null) {
                finishRegister();
            }else {
                Toast.makeText(getActivity(), R.string.submit_error, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
        }

        private void finishRegister() {

        }
    }

}
