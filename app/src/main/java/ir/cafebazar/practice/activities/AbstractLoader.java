package ir.cafebazar.practice.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.AsyncTaskLoader;
import android.view.View;

public abstract class AbstractLoader<D> extends AsyncTaskLoader<D> {

    private D data;
    private View progress;
    private View rootView;
    private Handler mHandler;
    private int last;
    private Bundle args;

    public AbstractLoader(Context context, Bundle args, int last) {
        this(context, null, null, args, last);
    }

    public AbstractLoader(Context context, View progress, View rootView, Bundle args, int last) {
        super(context);
        this.progress = progress;
        this.rootView = rootView;
        this.last = last;
        this.args = args;
        this.mHandler = new Handler();
    }

    @Override
    public final D loadInBackground() {

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                if (rootView != null) {
                    rootView.setVisibility(View.GONE);
                }
                if (progress != null) {
                    progress.setVisibility(View.VISIBLE);
                }
            }
        });

        this.data = queryServer(args, last);

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                if (progress != null) {
                    progress.setVisibility(View.GONE);
                }
                // if (rootView != null) {
                // rootView.setVisibility(View.VISIBLE);
                // }
            }
        });

        return this.data;
    }

    @Override
    protected void onStartLoading() {
        if (data != null) {
            deliverResult(data);
        } else {
            forceLoad();
        }
    }

    protected abstract D queryServer(Bundle args, int last);

}
