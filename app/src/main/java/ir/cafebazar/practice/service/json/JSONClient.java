package ir.cafebazar.practice.service.json;

import net.londatiga.android.instagram.InstagramRequest;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.List;

import ir.cafebazar.practice.broadcast.ApplicationData;

//TODO: this class mixes different functionalities and can be subdivided into separate classes, e.g.: JSON Client, JSONRPCClient, JSON Decoder, Cache, etc.
//TODO: implement cache
public class JSONClient {
    private JSONArray jsonData;


    public JSONArray sendRequest(String method, String count) {
        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>(1);

            params.add(new BasicNameValuePair("count", "30"));

            //TODO: to get all the items must set next_url as a parameter
//            params.add(new BasicNameValuePair("next_url", ""));

            InstagramRequest request = new InstagramRequest(ApplicationData.session.getAccessToken());
            String response			 = request.createRequest("GET", "/users/self/media/recent/", params);

            if (!response.equals("")) {
                JSONObject jsonObj  = (JSONObject) new JSONTokener(response).nextValue();
                jsonData	= jsonObj.getJSONArray("data");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return jsonData;
    }
}
