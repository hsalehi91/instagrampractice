package ir.cafebazar.practice.service;

import org.json.JSONException;

public interface Service {
    /**
     *
     * Provides latest image urls of a specific user self media, page by page, each page having an arbitrary number of image urls.
     * For a more detailed guide on how pagination works, see {@link}.
     *
     * @param page
     *            the page that has to be received by client
     * @return
     *            an array of urls of arbitrary-length
     */
    String[] getUserSelfMedia(String page) throws JSONException;
}
