package ir.cafebazar.practice.service.dummy;

import org.json.JSONException;

import ir.cafebazar.practice.service.Service;

public class DummyService implements Service {


    private void waitALittle() {
        try {
            int delay = 1000 + (int) (Math.random() * 1000);
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String[] getUserSelfMedia(String count) throws JSONException {
        return new String[0];
    }
}
