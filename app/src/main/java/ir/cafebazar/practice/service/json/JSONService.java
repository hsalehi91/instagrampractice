package ir.cafebazar.practice.service.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ir.cafebazar.practice.service.Service;

public class JSONService implements Service {

    @Override
    public String[] getUserSelfMedia(String count) throws JSONException {
        JSONClient client = new JSONClient();
        ArrayList<String> photoList = null;
        JSONArray jsonData = client.sendRequest("getUserSelfMedia", count);
        if(jsonData != null){
            int length = jsonData.length();

            if (length > 0) {
                photoList = new ArrayList<String>();

                for (int i = 0; i < length; i++) {
                    JSONObject jsonPhoto = jsonData.getJSONObject(i).getJSONObject("images").getJSONObject("low_resolution");

                    photoList.add(jsonPhoto.getString("url"));
                }
            }
        }

        if (photoList != null) {
            return photoList.toArray(new String[photoList.size()]);
        }
        else return null;
    }
}
