package ir.cafebazar.practice.service.dummy;

import java.util.Random;

public class DummyServiceUtil {

    static String[] userIcons = { "http://www.google.com/favicon.ico", "http://stackoverflow.com/favicon.ico",
            "http://wikipedia.org/favicon.ico", "http://www.yahoo.com/favicon.ico", "https://trello.com/favicon.ico",
            "http://findicons.com/favicon.ico", "FAKE_MALFORMED_URL" };

    static String[] imagesUrls = { "https://www.google.com/images/srpr/logo4w.png",
            "https://www.google.com/images/errors/robot.png",
            "http://www.tabnak.ir/client/themes/fa/main/img/l_logo.gif",
            "http://www.tabnak.ir/client/themes/fa/main/img/r_logo.gif",
            "http://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Wikipedia-logo-v2-fa.svg/135px-Wikipedia-logo-v2-fa.svg.png" };

    public static String generateRandomString(int length) {
        char[] chars = "اااااااااابببپتتثدددذرررزژسسسشکگلللممممننننووووههییییی".toCharArray();
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        int lastSpace = 0;
        for (int i = 0; i < length; i++) {
            if (lastSpace > 7 || (lastSpace > 1 && Math.random() < 0.25)) {
                builder.append(" ");
                lastSpace = 0;
            } else {
                char c = chars[random.nextInt(chars.length)];
                builder.append(c);
                lastSpace++;
            }
        }
        String output = builder.toString();
        return output;
    }

    public static String generateRandomText() {
        String result = "";
        int numberOfParagraphs = 3 + (int) (Math.random() * 4);
        for (int i = 0; i < numberOfParagraphs; i++) {
            int paragraphLength = 300 + (int) (200 * Math.random());
            result += generateRandomString(paragraphLength) + ".\n\n";
        }
        return result;
    }

    public static Integer[] generateRandomDiagramData(int length) {
        Integer[] result = new Integer[length];
        int previous = 300 + (int) (Math.random() * 200);
        for (int i = 0; i < length; i++) {
            result[i] = previous;
            previous += (int) (Math.random() * 40) - 20;
        }
        return result;
    }
}
