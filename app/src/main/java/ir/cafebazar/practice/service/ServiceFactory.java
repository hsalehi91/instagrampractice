package ir.cafebazar.practice.service;

import ir.cafebazar.practice.Constants;
import ir.cafebazar.practice.service.dummy.DummyService;
import ir.cafebazar.practice.service.json.JSONService;

public class ServiceFactory {

    private static Service service;

    static {
        initService();
    }

    public static Service getInstance() {
        return service;
    }

    private synchronized static void initService() {

        if (service != null) {
            return;
        }

        if (Constants.USE_DUMMY_SERVICE) {
            service = new DummyService();
        } else {
//            String serverUrl = Constants.SERVER_BASE_ADDRESS;
            service = new JSONService();
        }

    }
}
