package ir.cafebazar.practice.util;

import android.content.Context;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import ir.cafebazar.practice.R;
import ir.cafebazar.practice.activities.Refreshable;

public class OptionsMenuUtil {

    public static void addRefreshButton(Menu menu, final Refreshable refreshable) {
        MenuItem refreshMenuItem = menu.add("Refresh");
        refreshMenuItem.setIcon(R.drawable.ic_menu_refresh);
        refreshMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        refreshMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                refreshable.onRefreshButtonClick();
                return true;
            }

        });
    }

    public static void addLoginButton(Context context, Menu menu) {
        MenuItem loginMenuItem = menu.add("Login");
        loginMenuItem.setIcon(R.drawable.ic_popup_login);
        loginMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

}
