package ir.cafebazar.practice.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ArrayUtil {

    public static <T> T[] concat(T[] first, T[] second) {
        if (first == null) {
            return second;
        }
        if (second == null) {
            return first;
        }
        List<T> both = new ArrayList<T>(first.length + second.length);
        Collections.addAll(both, first);
        Collections.addAll(both, second);
        return both.toArray(first);
    }

    public static <T> boolean isEmpty(T[] array) {
        return (array == null || array.length == 0);
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] removeNulls(T[] array) {
        List<T> result = new ArrayList<T>();
        for (T t : array) {
            if (t != null) {
                result.add(t);
            }
        }
        return (T[]) result.toArray();
    }

    public static String encodeCSV(String[] input) {
        if (input == null || input.length == 0) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        for (String part : input) {
            builder.append(part + ",");
        }
        return builder.substring(0, builder.length() - 1);
    }

    public static String[] decodeCSV(String input) {
        if (input == null || input.length() == 0) {
            return new String[] {};
        }
        List<String> results = new ArrayList<String>();
        for (String part : input.split(",")) {
            if (part.trim().length() > 0) {
                results.add(part.trim());
            }
        }
        return results.toArray(new String[] {});
    }
}
