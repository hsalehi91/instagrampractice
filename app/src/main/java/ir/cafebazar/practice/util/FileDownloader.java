package ir.cafebazar.practice.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.os.Environment;
import android.util.Log;

public class FileDownloader {

    public static File downloadFile(String urlWithMd5) throws IOException {

        // extract md5 from url
        int hashMark = urlWithMd5.indexOf("#");
        String md5 = null;
        String urlString = urlWithMd5;
        if (hashMark > -1) {
            urlString = urlWithMd5.substring(0, hashMark);
            md5 = urlWithMd5.substring(hashMark + 1);
        }

        Log.d("FileDownloader", "Downloading file " + urlString + " with hash: " + md5);

        // prepare the output file
        String pathName = Environment.getExternalStorageDirectory() + "/download/";
        File path = new File(pathName);
        if (!path.exists()) {
            path.mkdir();
        }
        String fileName = extractFileName(urlString);
        File output = new File(path, fileName);
        Log.d("FileDownloader", "Saving to: " + output);
        if (output.exists()) {
            if (output.length() > 0 && verifyChecksum(output, md5)) {
                // the file has been prevoiusly downloaded and the checksum is correct!
                Log.d("FileDownloader", "File already exists. Download completed!");
                return output;
            } else {
                Log.d("FileDownloader", "File already exists, but seems corrupted. File Deleted!");
                output.delete();
                output = new File(path, fileName);
            }
        }
        FileOutputStream outputStream = new FileOutputStream(output);

        // make connection
        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.connect();
        InputStream inputStream = connection.getInputStream();

        // copy the input stream to output stream
        byte[] buffer = new byte[1024];
        while (true) {
            int len = inputStream.read(buffer);
            if (len <= 0) {
                break;
            }
            outputStream.write(buffer, 0, len);
        }
        outputStream.close();

        // check the checksum
        if (!verifyChecksum(output, md5) || output.length() == 0) {
            Log.d("FileDownloader", "File seems corrupted!");
            // the file is corrupted! delete it!
            output.delete();
            return null;
        }

        Log.d("FileDownloader", "Download finished successfully!");
        return output;
    }

    private static boolean verifyChecksum(File file, String md5) throws IOException {
        if (md5 == null || md5.length() == 0) {
            return true;
        }
        try {
            String computedMd5 = computeCheckSum(file);
            return md5.equalsIgnoreCase(computedMd5);
        } catch (NoSuchAlgorithmException e) {
            // skip checksum!
            return true;
        }
    }

    private static String computeCheckSum(File file) throws NoSuchAlgorithmException, IOException {
        MessageDigest digest = MessageDigest.getInstance("md5");

        byte[] buffer = new byte[1024];
        FileInputStream inputStream = new FileInputStream(file);
        while (true) {
            int len = inputStream.read(buffer);
            if (len <= 0) {
                break;
            }
            digest.update(buffer, 0, len);
        }
        inputStream.close();
        byte[] hashBytes = digest.digest();

        // Convert to hex
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hashBytes.length; i++) {
            hexString.append(Integer.toHexString(0xFF & hashBytes[i]));
        }
        String md5 = hexString.toString();
        return md5;
    }

    private static String extractFileName(String url) {
        int slashIndex = url.lastIndexOf('/');
        return url.substring(slashIndex + 1);
    }
}
