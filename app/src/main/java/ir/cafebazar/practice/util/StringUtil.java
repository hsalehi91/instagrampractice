package ir.cafebazar.practice.util;


import android.content.Context;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

import ir.cafebazar.practice.R;

public class StringUtil {

    private static final String PERSIAN_DIGITS = "۰۱۲۳۴۵۶۷۸۹";

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.00");

    public static String toPersianDigits(String input) {
        for (int i = 0; i < 10; i++) {
            input = input.replaceAll("" + i, "" + PERSIAN_DIGITS.charAt(i));
        }
        input = input.replaceAll("\\.", "\u066B");
        return input;
    }

    public static String formatPrice(double input) {
        int inputAsInt = (int) input;
        return toPersianDigits("" + inputAsInt);
    }

    public static String formatRate(double input) {
        return toPersianDigits(DECIMAL_FORMAT.format(input));
    }

    public static CharSequence toRelativeDate(Context context, Date date) {
        long diff = Calendar.getInstance().getTimeInMillis() - date.getTime();
        diff = Math.max(0, diff);

        diff /= 60 * 1000; // in minutes
        if (diff < 60) {
            diff = Math.min(1, diff);
            return context.getResources().getString(R.string.minutes_ago, toPersianDigits("" + diff));
        }

        diff /= 60; // in hours
        if (diff < 24) {
            return context.getResources().getString(R.string.hours_ago, toPersianDigits("" + diff));
        }

        diff /= 24; // in days
        if (diff < 30) {
            return context.getResources().getString(R.string.days_ago, toPersianDigits("" + diff));
        }

        diff /= 30; // in months
        if (diff < 12) {
            return context.getResources().getString(R.string.months_ago, toPersianDigits("" + diff));
        }

        diff = diff * 30 / 365; // in years
        diff = Math.max(0, diff);
        return context.getResources().getString(R.string.years_ago, toPersianDigits("" + diff));
    }
}
