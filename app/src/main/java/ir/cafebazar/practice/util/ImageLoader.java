package ir.cafebazar.practice.util;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.widget.ImageView;

public class ImageLoader {

    private final int REQUIRED_SIZE = 70;

    private final Map<String, Bitmap> cache;
    private final static Handler mHandler = new Handler();

    public ImageLoader() {
        cache = new HashMap<String, Bitmap>();
    }

    public void setImage(final String url, final ImageView imageView) {

        if (url == null || url.trim().length() == 0) {
            return;
        }

        if (cache.containsKey(url)) {
            imageView.setImageBitmap(cache.get(url));
        }

        Thread thread = new Thread() {

            @Override
            public void run() {

                final Bitmap bitmap = fetchImage(url);

                if (bitmap == null) {
                    return;
                }

                cache.put(url, bitmap);

                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        imageView.setImageBitmap(bitmap);

                    }
                });

            }
        };

        thread.start();

    }

    private Bitmap fetchImage(String url) {

        InputStream inputStream = downloadImage(url);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(inputStream, null, options);

        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE && options.outHeight / scale / 2 >= REQUIRED_SIZE) {
            scale *= 2;
        }

        options = new BitmapFactory.Options();
        options.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, options);

        return bitmap;
    }

    private InputStream downloadImage(String url) {

        try {

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet request = new HttpGet(url);
            HttpResponse response = httpClient.execute(request);
            return response.getEntity().getContent();

        } catch (Exception ex) {
            return null;
        }

    }
}