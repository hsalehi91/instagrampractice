package ir.cafebazar.practice.util.popUpMenu;

import android.content.Intent;
import android.graphics.drawable.Drawable;

public class MenuItem {

    private int itemId;
    private int parentId;
    private String title;
    private Drawable icon;
    private Intent intent;
    private Object object;
    private String iconUrl;
    private boolean isChecked = false;

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setObject(Object obj) {
        this.object = obj;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public void setIconUrl(String url) {
        this.iconUrl = url;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }

    public int getItemId() {
        return itemId;
    }

    public int getParentId() {
        return parentId;
    }

    public String getTitle() {
        return title;
    }

    public Drawable getIcon() {
        return icon;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public Intent getIntent() {
        return intent;
    }

    public Object getObject() {
        return object;
    }
}
